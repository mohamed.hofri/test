terraform {
  required_providers {
    docker = {
      source  = "kreuzwerker/docker"
      version = ">= 2.13.0"
    }
  }
}

provider "docker" {}

resource "docker_image" "nginx" {
  name         = "nginx:latest"
  keep_locally = false
  
}

resource "docker_image" "haproxy" {
  name         = "haproxy:latest"
  keep_locally = false
}

resource "docker_network" "private_network" {
  name = "net_ha"
  driver = "bridge"
  ipam_config {
    subnet  = "10.1.1.0/24"
    gateway = "10.1.1.20"
  }
}

resource "docker_container" "nginx1" {
  image = docker_image.nginx.image_id
  name = "nginx1"
  networks_advanced {
    name = docker_network.private_network.name
    ipv4_address = "10.1.1.12"
  }
  ports {
    internal = 80

  }
  volumes {
    host_path = "/home/user1/projet/projet_haute_dispo/index1.html"
    container_path = "/usr/share/nginx/html/index.html"
  }
  depends_on = [docker_network.private_network]
}
resource "docker_container" "nginx2" {
  image = docker_image.nginx.image_id
  name = "nginx2"
  networks_advanced {
    name = docker_network.private_network.name
    ipv4_address = "10.1.1.15"
  }
  ports {
    internal = 80
  }
  volumes {
    host_path = "/home/user1/projet/projet_haute_dispo/index2.html"
    container_path = "/usr/share/nginx/html/index.html"
  }
  depends_on = [docker_network.private_network]
}

resource "docker_container" "haproxy" {
  image = docker_image.haproxy.image_id
  name = "haproxy"
  networks_advanced {
    name = docker_network.private_network.name
    ipv4_address = "10.1.1.2"
  }
  ports {
  internal = 80
  external = 8383
  }

  command = ["haproxy", "-f", "/usr/local/etc/haproxy/haproxy.cfg"]
  
  volumes {
  host_path = "/home/user1/projet/projet_haute_dispo/haproxy.cfg"
  container_path = "/usr/local/etc/haproxy/haproxy.cfg"
  }
}



