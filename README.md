# Projet haute disponibilité : Déploiement d’une infrastructure avec Terraform/Docker


## Contexte 

Déploiement de manière automatisé des ressources suivantes :
- 2 serveurs Webs 
- 1 Ha proxy
- 1 réseau indépendant/Sécurisé pour lié les ressources

Le tout doit être préconfiguré dès la création des ressources


> lien de la présentation :
https://docs.google.com/presentation/d/1LXXBQhNczwQEwkwH3zR-hW4bcOQQODkM27mijRWZEZA/edit?usp=sharing


## Mise en place solution :


Dans notre cas nous avons choisi d'opter pour la téchnologie docker afin de mettre en place les différentes ressources nécessaire au projet (serveur web , proxy ...).
Les différents conteneur seront reliés entre eux par l'interméidiaire d'un réseau bridge.
De ce fait chaque conteneur aura sa propre adresse IP et pourra donc communiquer plus facilement avec les différentes briques de notre réseau.

Le tout sera bien évidement relié à notre machine physique.

Afin de facilité et d'automatiser la création des réssources nous allons utiliser l'outil Terraform pour créer nos différents conteneur ainsi que le subnet qui relie le tout.

Pour finir le script Terraform prend en compte le fichier de configuration du haproxy(haproxy.cfg) ainsi que les 2 pages d'accueil de nos serveur ngnix(index.hml),afin que ces derniers puisse être positionné dans le bon réportoire dés la création des coonteneurs docker.


## Avantage de la solution 

La solution mis en place permet de centraliser à la fois le deploiement et la configuration en local avant de déployer les ressources.
De ce fait la solution reste quand même modulation et adaptable au besoin des utilisateurs finaux.
